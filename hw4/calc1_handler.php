<?php
require_once "Dao.php";
session_start();
$dao = new Dao();
  $username = $_SESSION['username'] ;
  // Sanitize input. Using round in case of .00 entry
  $age = round($_POST['age']);
  $_SESSION['calc1']['age'] = $age;

  $_SESSION['calc1']['income'] = $_POST['income'];
  $temp1 = preg_replace('/[\$,]/', '', $_POST['income']);
  $temp1= floatval($temp1);
  $income = round($temp1);

  $_SESSION['calc1']['networth'] = $_POST['networth'];
  $temp2 = preg_replace('/[\$,]/', '', $_POST['networth']);
  $temp2 = floatval($temp2);
  $networth = round($temp2);

  $status = "";
  $average = "";
  $validated = true;

// Validate input
  if ($age <=0)
  {
    $_SESSION['error'][] = "Age cannot be negative or 0";
    $validated = false;
  }
  if ($income <=0)
  {
    $_SESSION['error'][] = "Income cannot be negative or 0";
    $validated = false;
  }
  if ($networth <=0)
  {
    $_SESSION['error'][] = "Net worth cannot be negative or 0";
    $validated = false;
  }
  if (!$validated) {
    header('Location: index.php');
    $_SESSION['validated'] = 'false';
    exit;
  }

  // Validated!
  $_SESSION['validated'] = 'true';

  $average = ($age * $income)/10;
  if ($networth >= $average *2){
    $status = "PAW";
    echo $status ;
  } elseif ($networth <= $average *.5) {
    $status = "UAW";
    echo $status ;
  } else {
    $status = "AAW";
    echo $status;
  }
 $_SESSION['calc1']["status"] =$status;

 //if logged in users have save to profile option
 if (isset($_POST['save'])) {
     $dao->saveCalc1($username, $age, $income, $networth, $status);

 }
  header('Location: index.php');
?>
