<?php
require_once "Dao.php";
session_start();
$dao = new Dao();
  $email = $_POST['email'];
  $username = $_POST['username'];
  $password = $_POST['password'];
  $_SESSION['presets']['email'] = $email;
  $_SESSION['presets']['username'] = $username;
  $_SESSION['presets']['password'] = $password;

  $error = array();
  $presets = array();
  $validated = true;

  // Check if email is empty
  if (empty($email)) {
    $_SESSION['error'][] = "Email is required";
    $validated = false;
  }
  // Sanitize email in case of unsafe characters
  $cleanemail = filter_var($email, FILTER_SANITIZE_EMAIL);
  // Validate email to check for valid format
  if (filter_var($cleanemail, FILTER_VALIDATE_EMAIL)){
      //See if email already exists
      if($dao->checkEmailExists($email) === true){
        $_SESSION['error'][]  = "User already exists for this email, please enter a new email or log in";
        $validated = false;
      }
  } else {
    $_SESSION['error'][] = "Email format is not valid, please enter a valid email";
    $validated = false;
  }
  // Check for valid username
  if (!preg_match('/^[A-Za-z0-9]{3,36}$/', $username))
  {
    $_SESSION['error'][] = "Username must be 3-36 characters long, letters and numbers only";
    $validated = false;
  }

  // Check that password is valid
  if (strlen($password ) <= '7') {
    $_SESSION['error'][] = "Your Password Must Contain At Least 8 Characters";
    $validated = false;
    }
  if(!preg_match("#[0-9]+#",$password)) {
    $_SESSION['error'][]  = "Your Password Must Contain At Least 1 Number";
    $validated = false;
    }
  if(!preg_match("#[A-Z]+#",$password)) {
    $_SESSION['error'][]  = "Your Password Must Contain At Least 1 Uppercase Letter";
    $validated = false;
    }
  if(!preg_match("#[a-z]+#",$password)) {
    $_SESSION['error'][] = "Your Password Must Contain At Least 1 Lowercase Letter";
    $validated = false;
    }

  if (!$validated) {
    header('Location: register.php');
    $_SESSION['validated'] = 'false';
    exit;
  }

  // Validated!
  $_SESSION['validated'] = 'true';

  unset($_SESSION['presets']);

  //Encrypting passwords
  $salt='zheshiwodesalt';
  $saltypassword = $password.$salt;
  $hashypassword = password_hash($saltypassword, PASSWORD_BCRYPT);

  $dao->saveUser($email, $username, $hashypassword);
  // $dao->getUser($username);
  $_SESSION['username'] = $username;
  $_SESSION["logged_in"]=true;
  header("Location: index.php");
  // header('Location: login.php');
?>
