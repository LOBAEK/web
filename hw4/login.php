<?php
session_start();

if (isset($_SESSION["logged_in"]) && $_SESSION["logged_in"]) {
    header("Location: index.php");
  }
?>

<html>
  <?php require_once "header.php"; ?>
  <?php require_once "nav.php"; ?>
  <head>
    <link href="reglog.css" type="text/css" rel="stylesheet" />
  </head>
  <br>
    <h2>Login</h2>

    <form method="post" class="login" action="login_handler.php">
    <br>
    <label for="username">Username:</label>
    <!-- <input type="text" id="username" name="username"/> -->
    <input type="text" name="username" pattern = "^[A-Za-z0-9]{3,36}$" title="Valid usernames are 3-36 characters long, letters and numbers only"value="<?php echo @$_SESSION['username']; ?>" required />
    <br>
    <label for="password">Password:</label>
    <input type="password" id="password" name="password" required/>
    <br>
    <button type="submit" class="loginbtn">Login</button>
    <div class="fadein" style="display:none">
    <?php
    if(isset($_SESSION['error'])) {
      echo '<div class="error">'.$_SESSION['error'].'</div>';
	    unset($_SESSION['error']);
    }
    ?>
    </div>
    <!-- Fade in! -->
    <script>
    $(function(){
        $('.fadein').fadeIn(1500);
    });
    </script>
    <br>
    <div class="signin">
    <h2>Don't have an account? <a href="register.php">Register</a>.</h2>
  </div>
<?php require_once "footer.php"; ?>
</html>
