<nav>
  <ul class="nav">
    <li><a href="about.php">About</a></li>
    <li><a href="index.php">Calculators</a>
    <ul>
      <li><a href="index.php">Wealth Accumulation Rate</a></li>
      <li><a href="calc2.php">When Will I Become a Millionaire?</a></li>
      <li><a href="calc3.php">How much should I save?</a></li>
      <li><a href="calc4.php">Just Starting Out</a></li>
    </ul>
  </li>
    <!-- <li class = "main"><a href="forumlanding.php">Forum</a></li> -->
    <li><a href="toplinks.php">Top Links</a></li>

    <li><a href="recommended.php">Recommended Reading</a></li>
    <?php  if (isset($_SESSION["logged_in"]) && $_SESSION["logged_in"]): ?>
      <li><a href="logout.php">Logout</a></li>
      <li><a href="profile.php">Profile</a></li>

    <?php else: ?>
      <li><a href="login.php">Login/Register</a></li>
    <?php endif; ?>
    <script>
    $(function(){
        $('a').each(function(){
            if ($(this).prop('href') == window.location.href) {
                $(this).addClass('active'); $(this).parents('li').addClass('active');
            }
        });
    });
  </script>
  </ul>
</nav>
