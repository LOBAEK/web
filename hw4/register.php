<?php
session_start();
$message = isset($_SESSION['message']) ? $_SESSION['message'] : '';
unset($_SESSION['message']);
?>
<html>
  <?php require_once "header.php"; ?>
  <?php require_once "nav.php"; ?>
  <head>
    <link href="reglog.css" type="text/css" rel="stylesheet" />
  </head>
  <br>
    <h2>Register</h2>
    <form method="post" class ="register" action="reg_handler.php">
    <br>
    <label for="email">Email:</label>
    <input type="email" id="email" name="email" value="<?php echo @$_SESSION['presets']['email']?>" required />

     <br>
    <label for="username">Username:</label>
    <input type="text" id="username" name="username" pattern = "^[A-Za-z0-9]{3,36}$" placeholder="3-36 characters long, letters and numbers only" title="3-36 characters long, letters and numbers only" value="<?php echo @$_SESSION['presets']['username']; ?>" required minlength="3" maxlength="36"/>
    <br>
    <label for="password">Password:</label>
    <input type="password" id="password" name="password" pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d!$%@#£€*?&amp]{8,}$"
           placeholder="Minimum 8 characters with at least one uppercase character, one lowercase character and one number." title="Please include at least 1 uppercase character, 1 lowercase character, and 1 number. Special characters optional" required minlength="8"/>
   <br>
    <br>
    <button type="submit" class="loginbtn">Register</button>
    </form>
  <div class="fadein" style="display:none">
      <?php
      if (isset($_SESSION['error'])) {
        foreach ($_SESSION['error'] as $error) {?>
        <div class="error <?php echo isset($_SESSION['validated']) ? $_SESSION['validated'] : '';?>">
          <?php echo $error; ?></div>
      <?php  }
      unset($_SESSION['error']);
      ?> </div>
      <?php } ?>
  </div>
  <!-- Fade in! -->
  <script>
  $(function(){
      $('.fadein').fadeIn(1500);
  });
  </script>
  <br>
    <div class="register">
    <h2>Already have an account? <a href="login.php">Log in</a>.</h2>
  </div>

<?php require_once "footer.php"; ?>
</html>
