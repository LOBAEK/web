<html>
  <?php require_once "header.php"; ?>
  <?php require_once "nav.php"; ?>
  <head>
    <link href="about.css" type="text/css" rel="stylesheet" />
  </head>
      <div class="image">
        <!-- <img src="images/aboutimg.jpg" alt="about" > -->
        <!-- <img src="images/about.png" alt="aboutwhy" > -->
        <a href="http://a.co/d/51sQCPN"><img src="images/about.png" title="The power of TIME"></a>
      </div>
      <div class="content">
        <br>
        <h2>Frequently Asked Questions</h2>
        <p class = "question">What does FIRE stand for? And what is it?</p>
        <p class = "answer">Financial Independence Retire Early. It's a catchy phrase that's been in the news lately but the general idea has been around for a while. The basics priniciples are:</p>
        <p class = "answer">Don't spend more than you make, don't live paycheck to paycheck.</p>
        <p class = "answer">Save an appropriate percentage of your income based on your retirement goals.</p>
        <p class = "answer">Know much your annual expenses are in order to calculate the principle you need before you can retire. </p>
        <p class = "question">Why would I want to retire early?</p>
        <p class = "answer">FIRE's not just for people that want to retire early, despite the acronym. It's for anybody that wants financial security so that they aren't tied to a job purely for the paycheck.</p>
        <p class = "answer">  Imagine going to work knowing that if you got fired or quit, you have a nest egg saved up that you don't have to work again if you don't want to! Financial freedom can increase your quality of life in
        many ways and allow you to seek other opportunities in life.</p>
        <p class = "question">How do I start?</p>
        <p class = "answer">Once you have a job and have earned income, you are eligible to contribute to a Roth IRA. That's a great place to start, the anuual contribution limit for 2018 is $5500. If your company offers a 401k, see if they offer a match. If they do, try to contribute at least that much so you can take advantage of the match, it's free money! Check out the <a href="recommended.php">recommended reading</a> for books to start with. </p>
        <hr>
        <h2>Resources</h2>
          <p class = "answer"><a href="https://www.youneedabudget.com">You Need a Budget- check it out, it's free for students!</a></p>
          <p class = "answer"><a href="https://www.mint.com/">Mint- A popular financial tracking tool</a></p>
          <p class = "answer"><a href="https://www.creditkarma.com/">CreditKarma- Keep track of your credit score</a></p>
          <p class = "answer"><a href="https://i.imgur.com/u0ocDRI.png">Spending Flow Chart</a></p>
      </div>
 <?php require_once "footer.php"; ?>
 </html>
