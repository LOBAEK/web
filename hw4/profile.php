<?php
require_once "Dao.php";
$dao = new Dao();
?>
<html>
  <?php
  require_once "header.php";
  require_once "nav.php";
  $username = $_SESSION['username'];
  ?>
  <head>
    <link href="profile.css" type="text/css" rel="stylesheet" />
  </head>
<br>
<body>
  <div>
    <div style="display: inline-block;" class = "profileinfo">
    <?php $info = $dao->getUserInfo($username);
        $info = (array_unique($info));?>
        <table class = "profile">
        	<tbody>
        		<tr>
        			<th colspan="2"> <?php echo $username;?>'s Profile</th>
        		</tr>
          </tr>
            <?php  foreach($info as $attr=>$val){?>
              <tr>  <td> <?php echo $attr; ?> </td>  <td> <?php echo $val; ?></td></tr>
            <?php	}?>
            </tr>
          	</tbody>
      </table></div>
      <div style="display: inline-block;" class = "profileinfo">
    <?php $info = $dao->getCalc1Info($username);
        $info = (array_unique($info));?>
        <table class = "profile">
          <tbody>
            <tr>
              <th colspan="2">Wealth Accumulation </th>
            </tr>
          </tr>
          <?php  foreach($info as $attr=>$val){?>
            <tr>  <td> <?php echo $attr; ?> </td>  <td> <?php echo $val; ?></td></tr>
          <?php	}?>
              <!-- <tr>  <td> household income</td>  <td> <?php echo array_unique($dao->gethouseholdincome($username));?></td></tr>
              <tr>  <td> current net worth </td>  <td> <?php echo $dao->getcurrentnw($username); ?></td></tr>
              <tr>  <td> wealth status </td>  <td> <?php echo $dao->getwealthstatus($username); ?></td></tr>
            </tr> -->
            </tbody>
      </table>
      </div>
      <div style="display: inline-block;" class = "profileinfo">
    <?php $info = $dao->getCalc2Info($username);
        $info = (array_unique($info));?>
        <table class = "profile">
          <tbody>
            <tr>
              <th colspan="2">Millionaire Projection</th>
            </tr>
          </tr>
            <?php  foreach($info as $attr=>$val){?>
              <tr>  <td> <?php echo $attr; ?> </td>  <td> <?php echo $val; ?></td></tr>
            <?php	}?>
            </tr>
            </tbody>
      </table>
    </div>
    <div style="display: inline-block;"class = "profileinfo">

      <?php $info = $dao->getCalc3Info($username);
          $info = (array_unique($info));?>
          <table class = "profile">
            <tbody>
              <tr>
                <th colspan="2">Savings</th>
              </tr>
            </tr>
              <?php  foreach($info as $attr=>$val){?>
                <tr>  <td> <?php echo $attr; ?> </td>  <td> <?php echo $val; ?></td></tr>
              <?php	}?>
              </tr>
              </tbody>
        </table>
      </div>
      <div style="display: inline-block;" class = "profileinfo">

        <?php $info = $dao->getCalc4Info($username);
            $info = (array_unique($info));?>
            <table class = "profile">
              <tbody>
                <tr>
                  <th colspan="2">Retirement</th>
                </tr>
              </tr>
                <?php  foreach($info as $attr=>$val){?>
                  <tr>  <td> <?php echo $attr; ?> </td>  <td> <?php echo $val; ?></td></tr>
                <?php	}?>
                </tr>
                </tbody>
          </table>
  </div>
</div>
  </body>
  <?php require_once "footer.php"; ?>
</html>
