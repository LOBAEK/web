<?php
require_once "Dao.php";
session_start();
$dao = new Dao();
  $username = $_SESSION['username'] ;
  $_SESSION['calc2']['savings'] = $_POST['savings'];
  $_SESSION['calc2']['deposit'] = $_POST['deposit'];
  $_SESSION['calc2']['interest'] = $_POST['interest'];

  // Sanitize input. Strip commas and $. Using round in case of .00 entry
  $temp1 = preg_replace('/[\$,]/', '', $_POST['savings']);
  $temp1= floatval($temp1);
  $savings= round($temp1);

  $temp2 = preg_replace('/[\$,]/', '', $_POST['deposit']);
  $temp2= floatval($temp2);
  $deposit= round($temp2);

  $temp3 = preg_replace('/[\%,]/', '', $_POST['interest']);
  $temp3= floatval($temp3);
  $interest= round($temp3);

  $validated = true;

  $yearstomil= "";

  // a = value of future investment
  // p = principle investment
  // r = interest rate as decimal
  // n = number of times compounded
  // t = number of years
  // m = monthly additional investment

  $a = 1000000;
  // $a= 23763.28;
  // - - - - - - - - - - - - - Frequency Selector - - - - - - - - - - - - -
  // frequency currently hardcoded to monthly, add other options if time
  $freq = 12;
  //assuming monthly compounding
  $n = 12;



  // Validate input
  if($savings > 0)
  {
    $p= $savings;
  } else {
    $_SESSION['error'][] = "Savings cannot be negative or 0";
    $validated = false;
  }
  if($deposit > 0)
  {
    $m= $deposit;
  } else {
    $_SESSION['error'][] = "Deposit amount cannot be negative or 0";
    $validated = false;
  }
  if($interest > 0)
  {
    $r= $interest / 100;
  } else {
    $_SESSION['error'][] = "Interest cannot be negative or 0";
    $validated = false;
  }

  // Calculate how many years to $1,000,000
  $t = round(log(($a*$r + $m*$n)/($p*$r + $m*$n))/($n*log(1+($r/$n))));
  $_SESSION['calc2']['years'] = $t;

  if (!$validated) {
    header('Location: calc2.php');
    $_SESSION['validated'] = 'false';
    exit;
  }

  // Validated!
  $_SESSION['validated'] = 'true';

  //if logged in users have save to profile option
  if (isset($_POST['save']) && $validated) {
         # Save-button was clicked
         $dao->saveCalc2($username, $p, $freq, $m, $r, $t);
     }
  // echo $status;
  header('Location: calc2.php');
?>
