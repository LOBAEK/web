<html>
  <?php require_once "header.php"; ?>
  <?php require_once "nav.php"; ?>
  <?php unset($_SESSION['calc4'])?>
  <?php unset($_SESSION['calc2'])?>
  <?php unset($_SESSION['calc3'])?>
  <body>
    <head>
      <link href="containers.css" type="text/css" rel="stylesheet" />
    </head>
    <br>

      <h2>What type of wealth accumulator are you? (According to the formula from The Millionaire Next Door)</h2>
        <div class="container">
        <fieldset>
            <legend>Let's find out!</a></legend>
            <div class="wealthinfo">
                <div class="wealthimage"><img src="images/paw.JPG" /></div>
                <div class="acronym">Prodigious Accumulator of Wealth</div>
                <div class="definition">Net worth is greater than 2 times your expected net worth.</div>
            </div>
          <br>
            <div class="wealthinfo">
                <div class="wealthimage"><img src="images/aaw.JPG" /></div>
                <div class="acronym">Average Accumulator of Wealth</div>
                <div class="definition">Net worth is between 1/2 and 2 times your expected net worth.</div>
            </div>
          <br>
            <div class="wealthinfo">
                <div class="wealthimage"><img src="images/uaw.JPG" /></div>
                <div class="acronym">Under Accumulator of Wealth</div>
                <div class="definition">Networth is less than 1/2 your expected networth.</div>
            </div>
            <form method="post" class ="calc1" action="calc1_handler.php">
              <!-- <form action="http://webster.cs.washington.edu/params.php"> -->

            <label for="age">Current Age:</label>
            <input type="number" id="age" name="age"
                   value="<?php echo @$_SESSION['calc1']['age']; ?>"/>
          <br>
          <label for="income">Pretax Annual Household Income:</label>
            <input type="text" id="income" name="income" data-type="currency"
                   value="<?php echo @$_SESSION['calc1']['income']; ?>"/>
            <br>
          <label for="networth">Current Net Worth:</label>
            <input type="text" id="networth" name="networth" data-type="currency"
                   value="<?php echo @$_SESSION['calc1']['networth']; ?>"/>
            <br>
            <!-- - - - - - - - - - - - - - BUTTONS - - - - - - - - - - - - - -->
            <input type="submit" class="calcbtn" name="calc" value="Calculate">
            <?php  if (isset($_SESSION["logged_in"]) && $_SESSION["logged_in"]): ?>
              <input type="submit" class="savebtn" name="save" value="Calculate and Save to Profile">
            <?php endif; ?>
          </form>
          <!-- - - - - - - - - - - - - - Errors- - - - - - - - - - - - - -->
          <?php
          if (isset($_SESSION['error'])) {
            foreach ($_SESSION['error'] as $error) {?>
            <div class="error <?php echo isset($_SESSION['validated']) ? $_SESSION['validated'] : '';?>">
              <?php echo $error; ?></div>
          <?php  }
          unset($_SESSION['error']);
          ?> </div>
        <?php } else {?>
            <!-- - - - - - - - - - - - - - RESULTS- - - - - - - - - - - - - -->
            <fieldset>
                <legend>Results</legend>
                <p>
                <?php  if ($_SESSION['calc1']["status"] == "PAW"): ?>
                  <div class="definition">You are a PAW or a Prodigious Accumulator of Wealth </div></p>
                  <div class="center">
                    <img src="images/paw.JPG" alt="PAW">
                  </div>
                <?php elseif ($_SESSION['calc1']["status"] == "UAW"): ?>
                  <div class="definition">You are a UAW or an Under Accumulator of Wealth </div></p>
                  <div class="center">
                    <img src="images/uaw.JPG" alt="UAW">
                  </div>
                <?php elseif ($_SESSION['calc1']["status"] == "AAW"): ?>
                  <div class="definition">You are a AAW or an Average Accumulator of Wealth </div></p>
                  <div class="center">
                    <img src="images/aaw.JPG" alt="AAW">
                  </div>
                <?php else:?>
                  <div class="acronym">What type of accumulator are you?</div> </p>
                  <div class="center">
                    <img src="images/what.png" alt="what">
                  </div>
                <?php endif; ?>
                <?php }?>
              <br>
            </fieldset>
        </fieldset>
  </div>
      <div class="image">
        <img src="images/calc1img.png" alt="Wealth Chart" >
      </div>
</div>

<!-- </div> -->
 </body>
<?php require_once "footer.php"; ?>
</html>
