<?php
require_once "Dao.php";
session_start();
$dao = new Dao();
  $username = $_SESSION['username'];
  // Sanitize input. Using round in case of .00 entry
  $age = round($_POST['age']);
  $targetage = round($_POST['targetage']);
  $_SESSION['calc3']['age'] = $age;
  $_SESSION['calc3']['targetage'] = $targetage;

  $_SESSION['calc3']['networth'] = $_POST['networth'];
  // Sanitize input. Strip commas and $. Using round in case of .00 entry
  $temp1 = preg_replace('/[\$,]/', '', $_POST['networth']);
  $temp1= floatval($temp1);
  $networth= round($temp1);

  $_SESSION['calc3']['interest'] = $_POST['interest'];
  $temp2 = preg_replace('/[\%,]/', '', $_POST['interest']);
  $temp2= floatval($temp2);
  $interest= round($temp2);

  $_SESSION['calc3']['targetnw'] = $_POST['targetnw'];
  $temp3 = preg_replace('/[\$,]/', '', $_POST['targetnw']);
  $temp3= floatval($temp3);
  $targetnw= round($temp3);

  $validated = true;

  // a = value of future investment
  // p = principle investment
  // r = interest rate as decimal
  // n = number of times compounded
  // t = number of years
  // m = monthly additional investment

  $saveamountmonth= "";
  //assuming monthly compounding
  $n = 12;

  // Validate input
    if ($age < 0)
    {
      $_SESSION['error'][] = "Age cannot be negative or 0";
      $validated = false;
    } elseif ($targetage < 0) {
      $_SESSION['error'][] = "Target age cannot be negative or 0";
      $validated = false;
    } elseif ($targetage < $age) {
      $_SESSION['error'][] = "Target age cannot be younger than current age";
      $validated = false;
    } else{
      $t = $targetage - $age;
    }

    if ($networth >0)
    {
      $p=$networth;
    } else {
      $_SESSION['error'][] = "Net worth cannot be negative or 0";
      $validated = false;
    }

    if ($targetnw <0)
    {
      $_SESSION['error'][] = "Target net worth cannot be negative or 0";
      $validated = false;
    }elseif($targetnw <=$networth){
      $_SESSION['error'][] = "Target net worth cannot be less than current net worth";
      $validated = false;
    } else {
      $a=$targetnw;
    }

    if($interest > 0)
    {
      $r= $interest / 100;
    } else {
      $_SESSION['error'][] = "Interest cannot be negative or 0";
      $validated = false;
    }

    if (!$validated) {
      header('Location: calc3.php');
      $_SESSION['validated'] = 'false';
      exit;
    }

// Calculate how much to contribute to reach target net worth by target age
  $dividend = ($a*$r)- ( ($p*$r) * (pow( (1+($r/$n)) , ($n*$t) )));
  $divisor = $n * (pow((1+($r/$n)), ($n*$t))-1);

  $saveamountmonth = round($dividend/$divisor);
  $_SESSION['calc3']['savethismuch'] = $saveamountmonth;
//  -- - - - - - - - - - - - - - RESULTS-  SAVE THIS TO DATABASE!- - - - - - - - - - - - --


// Validated!
//if logged in users have save to profile option
if (isset($_POST['save'])) {
       # Save-button was clicked
       $dao->saveCalc3($username, $age, $p, $r, $a, $targetage, $saveamountmonth);
   }
  // echo $status;
  header('Location: calc3.php');
?>
