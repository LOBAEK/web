<?php
class Dao {

  private $host = "us-cdbr-iron-east-01.cleardb.net";
  private $db = "heroku_d7dbc54b5ec249b";
  private $user = "bf4059e6cbf916";
  private $pass = "eb443e97";

  public function getConnection () {
       $conn= new PDO("mysql:host={$this->host};dbname={$this->db}", $this->user,
          $this->pass);
    return $conn;
  }

  public function saveUser ($email, $username, $password) {
    $conn = $this->getConnection();
    $saveQuery =
        "INSERT INTO user
        (email, username, thepassword)
        VALUES
        (:email, :uname, :pass)";
    $q = $conn->prepare($saveQuery);
    $q->bindParam(":email", $email);
    $q->bindParam(":uname", $username);
    $q->bindParam(":pass", $password);
    $q->execute();
  }

  public function getUser($username){
    // echo $username;
    $conn = $this->getConnection();
    $stmt = $conn->prepare("SELECT * FROM user WHERE username = :uname");
    $stmt->bindParam(":uname", $username);
    $stmt->execute();
    // echo $stmt->fetch();
    return $stmt->fetch();
}

  public function getUserInfo($username){
    $conn = $this->getConnection();
    $stmt = $conn->prepare("SELECT username, email, age FROM user WHERE username = :uname");
    $stmt->bindParam(":uname", $username);
    $stmt->execute();
    return $stmt->fetch();
  }
  public function gethouseholdincome($username){
    $conn = $this->getConnection();
    $stmt = $conn->prepare("SELECT householdincome FROM user WHERE username = :uname");
    $stmt->bindParam(":uname", $username);
    $stmt->execute();
    return $stmt->fetch();
  }
  public function getcurrentnw($username){
    $conn = $this->getConnection();
    $stmt = $conn->prepare("SELECT currentnetworth FROM user WHERE username = :uname");
    $stmt->bindParam(":uname", $username);
    $stmt->execute();
    return $stmt->fetch();
  }
  public function getwealthstatus($username){
    $conn = $this->getConnection();
    $stmt = $conn->prepare("SELECT wealthstatus FROM user WHERE username = :uname");
    $stmt->bindParam(":uname", $username);
    $stmt->execute();
    return $stmt->fetch();
  }

  public function getCalc1Info($username){
    $conn = $this->getConnection();
    $stmt = $conn->prepare("SELECT age, householdincome, currentnetworth, wealthstatus FROM user WHERE username = :uname");
    $stmt->bindParam(":uname", $username);
    $stmt->execute();
    return $stmt->fetch();
  }
  public function getCalc2Info($username){
    $conn = $this->getConnection();
    $stmt = $conn->prepare("SELECT currentsavings, depositamount, yearstomil FROM user WHERE username = :uname");
    $stmt->bindParam(":uname", $username);
    $stmt->execute();
    return $stmt->fetch();
  }
  public function getCalc3Info($username){
    $conn = $this->getConnection();
    $stmt = $conn->prepare("SELECT networth, interest, targetnw, targetage, saveamountmonth FROM user WHERE username = :uname");
    $stmt->bindParam(":uname", $username);
    $stmt->execute();
    return $stmt->fetch();
  }
  public function getCalc4Info($username){
    $conn = $this->getConnection();
    $stmt = $conn->prepare("SELECT  income, percentage, match, roth, totalcont FROM user WHERE username = :uname");
    $stmt->bindParam(":uname", $username);
    $stmt->execute();
    return $stmt->fetch();
  }

  public function checkEmailExists($email){
    $conn = $this->getConnection();
    $query = "SELECT * FROM user WHERE email = :emailaddy";
    $q = $conn->prepare($query);
    $q->bindParam(":emailaddy",$email);
    $q->execute();
    if($q->fetch()){
      return true;
    }else{
      return false;
    }
  }

  public function loggedIn($username, $password){
    $conn = $this->getConnection();
    $query = "SELECT * FROM user WHERE username = :uname && thepassword = :pass";
    $q = $conn->prepare($query);
    $q->bindParam(":uname",$username);
    $q->bindParam(":pass",$password);
    $q->execute();

    if($q->fetch()){
      return true;
    }else{
      return false;
    }
  }

  public function saveCalc1 ($username, $age, $income, $networth, $status) {
    $conn = $this->getConnection();
    $saveQuery =
        "UPDATE user
        SET age='$age', householdincome='$income', currentnetworth='$networth', wealthstatus='$status'
        WHERE username = '$username'";
    $q = $conn->prepare($saveQuery);
    $q->execute();
  }

  public function saveCalc2 ($username, $savings, $freq, $deposit, $interest, $years) {
    $conn = $this->getConnection();
    $saveQuery =
        "UPDATE user
        SET currentsavings='$savings', frequency = '$freq', depositamount='$deposit', annualinterest='$interest', yearstomil='$years'
        WHERE username = '$username'";
    $q = $conn->prepare($saveQuery);
    $q->execute();
  }

  public function saveCalc3 ($username, $age, $networth, $interest, $targetnw, $targetage, $saveamountmonth) {
    $conn = $this->getConnection();
    $saveQuery =
        "UPDATE user
        SET age='$age', currentnetworth='$networth', interest='$interest', targetnetworth='$targetnw', targetage='$targetage', saveamountmonth='$saveamountmonth'
        WHERE username = '$username'";
    $q = $conn->prepare($saveQuery);
    $q->execute();
  }

  public function saveCalc4part1 ($username, $age, $income, $percentage, $match, $roth, $totalcont) {
    $conn = $this->getConnection();
    $saveQuery =
        "UPDATE user
        SET age='$age', householdincome='$income', fourkcont='$percentage', fourkmatch='$match', rothcont='$roth', annualcont='$totalcont'
        WHERE username = '$username'";
    $q = $conn->prepare($saveQuery);
    $q->execute();
  }

  public function saveCalc4part2 ($username, $initial, $interest, $years, $retage, $totalvalue) {
    $conn = $this->getConnection();
    $saveQuery =
        "UPDATE user
        SET startbalance='$intial', interest='$interest', yearstocontribute='$years', ageatretirement='$retage', totalvalue='$totalvalue'
        WHERE username = '$username'";
    $q = $conn->prepare($saveQuery);
    $q->execute();
  }


}
