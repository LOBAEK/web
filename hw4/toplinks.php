<html>
  <?php require_once "header.php"; ?>
  <?php require_once "nav.php"; ?>
  <head>
    <link href="about.css" type="text/css" rel="stylesheet" />
  </head>
<?php
$url = 'https://www.reddit.com/r/financialindependence/top/.json?t=week'; // path to your JSON file
$data = file_get_contents($url); // put the contents of the file into a variable
$links = json_decode($data, true); // decode the JSON feed
?>
<body>
  <br>
  <div>
<table class = "posts">
	<tbody>
		<tr>
			<th colspan="2">Top posts of the week from the Financial Independence subreddit</th>
		</tr>
  <?php foreach($links['data']['children'] as $child) { ?>
        <tr>
          <?php
          $dateInUTC=$child['data']['created_utc'];
          $date = date('D M j G:i', $dateInUTC);
          ?>
        <tr>  <td> <?php echo $date; ?> </td>
        <?php $link_address = $child['data']['url']; ?>
        <td><a href="<?php echo $link_address;?>"> <?php echo $child['data']['title']; ?>  </a></td></tr>

		<?php	}?>
        </tr>
	</tbody>
</table>
</div>
   </body>
<?php require_once "footer.php"; ?>
</html>
