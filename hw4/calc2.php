<html>
  <?php require_once "header.php"; ?>
  <?php require_once "nav.php"; ?>
  <?php unset($_SESSION['calc1'])?>
  <?php unset($_SESSION['calc4'])?>
  <?php unset($_SESSION['calc3'])?>
    <body>
      <head>
        <link href="containers.css" type="text/css" rel="stylesheet" />
      </head>
      <br>
        <h2>Saving already? Calculate when you will hit $1,000,000 in net worth based on current savings and rate of monthly deposits.</h2>
        <div>
          <div class="container">
          <fieldset>
              <legend>When Will I Become A Millionaire?</legend>
              <form method="post" class ="calc2" action="calc2_handler.php">

              <label for="Savings">Current Savings:</label>
              <input type="text" id="savings" name="savings" data-type="currency"
                    value="<?php echo @$_SESSION['calc2']['savings']; ?>"/>
          	         <br>
                  <!--    - - - - - - - - - - - - - Frequency Selector - - - - - - - - - - - - - -->
              <!-- <select>
                <option value="yearly">Yearly</option>
                <option value="monthly">Monthly</option>
                <option value="weekly">Weekly</option>
          	</select> -->
          	<label for="deposit">Monthly deposit amount:</label>
              <input type="text" id="deposit" name="deposit" data-type="currency"
                     value="<?php echo @$_SESSION['calc2']['deposit']; ?>"/>
              <br>
              <label for="interest">Interest Rate:</label>
              <input type="text" id="interest" name="interest" data-type="percent"
                     value="<?php echo @$_SESSION['calc2']['interest']; ?>"placeholder="6%"/>

          	<br>

            <!-- - - - - - - - - - - - - - BUTTONS - - - - - - - - - - - - - -->
            <input type="submit" class="calcbtn" name="calc" value="Calculate">
            <?php  if (isset($_SESSION["logged_in"]) && $_SESSION["logged_in"]): ?>
            <input type="submit" class="savebtn" name="save" value="Calculate and Save to Profile">
          <?php endif; ?>
        </form>
        <!-- - - - - - - - - - - - - - Errors- - - - - - - - - - - - - -->
        <?php
        if (isset($_SESSION['error'])) {
          foreach ($_SESSION['error'] as $error) {?>
          <div class="error <?php echo isset($_SESSION['validated']) ? $_SESSION['validated'] : '';?>">
            <?php echo $error; ?></div>
        <?php  }
        unset($_SESSION['error']);
        ?> </div>
      <?php } else {?>
               <!-- - - - - - - - - - - - - - RESULTS- - - - - - - - - - - - - -->
               <?php if (isset($_SESSION['calc2']['years'])) {?>
                <p class = "sansserif">You will hit a net worth of $1,000,000 in
                  <?php echo$_SESSION['calc2']['years']; ?>
                  years!
                    <?php } ?>
                </p>
          <?php }?>
               <br>
          </fieldset>
    </div>
        <div class="image">
          <img src="images/calc2img.jpg" alt="Savings Chart" >
        </div>
</div>
   </body>
<?php require_once "footer.php"; ?>
</html>
