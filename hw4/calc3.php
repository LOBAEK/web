<html>
  <?php require_once "header.php"; ?>
  <?php require_once "nav.php"; ?>
  <?php unset($_SESSION['calc1'])?>
  <?php unset($_SESSION['calc2'])?>
  <?php unset($_SESSION['calc4'])?>
    <body>
      <head>
        <link href="containers.css" type="text/css" rel="stylesheet" />
      </head>
      <br>
        <h2>How much should I save to hit a target net worth by a certain age?</h2>
        <div>
          <div class="container">
          <fieldset>
              <legend>How Much to Save?</legend>
              <form method="post" class ="calc3" action="calc3_handler.php">

                  <label for="age">Current Age:</label>
                  <input type="number" id="age" name="age"
                         value="<?php echo @$_SESSION['calc3']['age']; ?>"/>
              	<br>
              	<label for="networth">Current Net Worth:</label>
                  <input type="text" id="networth" name="networth" data-type="currency"
                         value="<?php echo @$_SESSION['calc3']['networth']; ?>"/>
                <br>
                  <label for="interest">Interest Rate:</label>
                  <input type="text" id="interest" name="interest" data-type="percent"
                         value="<?php echo @$_SESSION['calc3']['interest'] ; ?>"/>
                <br>
                  <label for="targetnw">Target Net Worth:</label>
                  <input type="text" id="targetnw" name="targetnw" data-type="currency"
                         value="<?php echo @$_SESSION['calc3']['targetnw'] ; ?>"/>
              	<br>
                  <label for="targetage">by age:</label>
                  <input type="number" id="targetage" name="targetage"
                         value="<?php echo @$_SESSION['calc3']['targetage']; ?>"/>
              	<br>
                <!-- - - - - - - - - - - - - - BUTTONS - - - - - - - - - - - - - -->
                <input type="submit" class="calcbtn" name="calc" value="Calculate">
                <?php  if (isset($_SESSION["logged_in"]) && $_SESSION["logged_in"]): ?>
                <input type="submit" class="savebtn" name="save" value="Calculate and Save to Profile">
                <?php endif; ?>
                </form>
                <!-- - - - - - - - - - - - - - Errors- - - - - - - - - - - - - -->
                <?php
                if (isset($_SESSION['error'])) {
                  foreach ($_SESSION['error'] as $error) {?>
                  <div class="error <?php echo isset($_SESSION['validated']) ? $_SESSION['validated'] : '';?>">
                    <?php echo $error; ?></div>
                <?php  }
                unset($_SESSION['error']);
                ?> </div>
              <?php } else {?>
                <!-- - - - - - - - - - - - - - RESULTS- - - - - - - - - - - - - -->
                <?php
                if (isset($_SESSION['calc3']['savethismuch'])) {?>
                <p><div class="acronym">Save $<?php echo$_SESSION['calc3']['savethismuch']; ?> a month
                 to reach <?php echo @$_SESSION['calc3']['targetnw']; ?> by <?php echo @$_SESSION['calc3']['targetage']; ?>!
               <?php } ?>
             </div></p>
         <?php }?>
                <br>
              </fieldset>
    </div>
        <div class="image">
          <a href="https://www.valuewalk.com/2016/04/savings-rate-by-country/"><img src="images/calc3img.png" alt="Savings for Retirement" >
        </div>
</div>
   </body>
<?php require_once "footer.php"; ?>
</html>
