<html>
  <?php require_once "header.php"; ?>
  <?php require_once "nav.php"; ?>
  <?php unset($_SESSION['calc1'])?>
  <?php unset($_SESSION['calc2'])?>
  <?php unset($_SESSION['calc3'])?>
    <body>
      <head>
        <link href="containers.css" type="text/css" rel="stylesheet" />
      </head>
      <br>
        <h2>Lets do some calculations! Assuming Annual Compound Interest</h2>
        <div>
          <!-- <br> -->
          <div class="container">
          <fieldset>
              <legend>Calculations!</legend>
              <form method="post" class ="calc4" action="calc4_handler.php">

                  <label for="age">Current Age:</label>
                  <input type="number" id="age" name="age"
                          value="<?php echo @$_SESSION['calc4']['age']; ?>" />
              	<br>
              	<label for="income">Household Income:</label>
                  <input type="text" id="income" name="income" data-type="currency"
                          value="<?php echo @$_SESSION['calc4']['income']; ?>" />
                  <br>
                  <label for="percentage">401k Percentage:</label>
                  <input type="text"id="percentage" name="percentage" data-type="percent"
                          value="<?php echo @$_SESSION['calc4']['percentage']; ?>"/>
              	<br>
                  <label for="match">401k Match:</label>
                  <input type="text" id="match" name="match" data-type="percent"
                          value="<?php echo @$_SESSION['calc4']['match']; ?>"/>
              	<br>
                  <label for="roth">Roth Contribution (2018 Max: $5500):</label>
                  <input type="text" id="roth" name="roth" data-type="currency"
                          value="<?php echo @$_SESSION['calc4']['roth']; ?>"/>
              	<br>
                <!-- - - - - - - - - - - - - - BUTTONS - - - - - - - - - - - - - -->
                <input type="submit" class="calcbtn" name="calc1" value="Calculate">
                <?php  if (isset($_SESSION["logged_in"]) && $_SESSION["logged_in"]): ?>
                <input type="submit" class="savebtn" name="save1" value="Calculate and Save to Profile">
                <?php endif; ?>
                <!-- - - - - - - - - - - - - - Errors- - - - - - - - - - - - - -->
                <?php
                if (isset($_SESSION['error'])) {
                  foreach ($_SESSION['error'] as $error) {?>
                  <div class="error <?php echo isset($_SESSION['validated']) ? $_SESSION['validated'] : '';?>">
                    <?php echo $error; ?></div>
                <?php  }
                unset($_SESSION['error']);
                ?> </div>
              <?php } else {?>
                <br>
                <!-- - - - - - - - - - - - - - RESULTS- - - - - - - - - - - - - -->
                <br>
                <?php
                if (isset($_SESSION['calc4']['totalcont'])) { ?>
                  <label for="interest">Starting balance (existing savings)</label>
                  <input type="text" id="initial" name="initial" data-type="currency"
                         value="<?php echo @$_SESSION['calc4']['initial']; ?>"/>
                <br>
                <label for="totalcont">Total Annual Contributions:</label>
                <!-- <input type="text" id="totalcont" name="totalcont" data-type="calculation" -->
                <?php setlocale(LC_MONETARY, 'en_US'); ?>
                <div class="results"> <?php echo money_format('%(#10n', @$_SESSION['calc4']['totalcont']). "\n"; ?> </div>
                <br>
                  <label for="interest">Interest Rate:</label>
                  <input type="text" id="interest" name="interest" data-type="percent"
                         value="<?php echo @$_SESSION['calc4']['interest']; ?>"/>
              	<br>
                  <label for="years">Years to contribute:</label>
                  <input type="number" id="years" name="years"
                         value="<?php echo @$_SESSION['calc4']['years']; ?>"/>
              	<br>
                  <label for="retage">Age at Retirement:</label>
                  <input type="number" id="retage" name="retage"
                         value="<?php echo @$_SESSION['calc4']['retage']; ?>"/>
              	<br>
                <?php
                if (isset($_SESSION['calc4']['totalvalue'])) { ?>
                  <label for="totalvalue">Total Value at retirement:</label>
                  <div class="results"> <?php echo money_format('%(#10n', @$_SESSION['calc4']['totalvalue']). "\n"; ?> </div>
                <?php } ?>
              <br>
              <?php $_SESSION['calc4']['round2']= true; ?>
              <!-- - - - - - - - - - - - - - BUTTONS - - - - - - - - - - - - - -->
              <input type="submit" class="calcbtn" name="calc2" value="Calculate">
              <?php  if (isset($_SESSION["logged_in"]) && $_SESSION["logged_in"]): ?>
              <input type="submit" class="savebtn" name="save2" value="Calculate and Save to Profile">
              <?php endif; ?>
                 <?php }?>
               <?php } ?>
              </fieldset>
    </div>
        <div class="image">
          <a href="https://www.reddit.com/r/personalfinance/comments/4gdlu9/how_to_prioritize_spending_your_money_a_flowchart/"><img src="images/calc4img.png" alt="Savings for Retirement" >
        </div>
</div>
   </body>
<?php require_once "footer.php"; ?>
</html>
