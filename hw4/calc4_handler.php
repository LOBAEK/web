<?php
require_once "Dao.php";
session_start();
$dao = new Dao();
  $username = $_SESSION['username'];
  // if (isset($_POST['calc1'])) {
  // Sanitize input. Using round in case of .00 entry
  $age = round($_POST['age']);
  $_SESSION['calc4']['age'] = $age;

  $_SESSION['calc4']['income'] = $_POST['income'];
  $temp1 = preg_replace('/[\$,]/', '', $_POST['income']);
  $temp1= floatval($temp1);
  $income= round($temp1);

  $_SESSION['calc4']['percentage'] = $_POST['percentage'];
  $temp2 = preg_replace('/[\%,]/', '', $_POST['percentage']);
  $temp2= floatval($temp2);
  $percentage= round($temp2);

  $_SESSION['calc4']['match'] = $_POST['match'];
  $temp3 = preg_replace('/[\%,]/', '', $_POST['match']);
  $temp3= floatval($temp3);
  $match= round($match);

  $_SESSION['calc4']['roth'] = $_POST['roth'];
  $temp4 = preg_replace('/[\$,]/', '', $_POST['roth']);
  $temp4= floatval($temp4);
  $roth= round($temp4);

  $validated = true;

  $saveamountmonth= "";
  //assuming monthly compounding
  $n = 12;
  // a = value of future investment
  // p = principle investment
  // r = interest rate as decimal
  // n = number of times compounded
  // t = number of years
  // m = monthly additional investment

  // Validate input
    if ($age < 0)
    {
      $_SESSION['error'][] = "Age cannot be negative or 0";
      $validated = false;
    }
    if ($income <0)
    {
      $_SESSION['error'][] = "Income cannot be negative or 0";
      $validated = false;
    }

    if ($percentage>=0)
    {
      $percentage= $percentage / 100;
    } else {
      $_SESSION['error'][] = "401k contribution percentage cannot be negative";
      $validated = false;
    }
    if ($match>=0)
    {
      $match= $match / 100;
    } else {
      $_SESSION['error'][] = "401k match percentage cannot be negative";
      $validated = false;
    }
    if ($roth< 0)
    {
      $_SESSION['error'][] = "Roth Contribution cannot be negative";
      $validated = false;
    }
    $totalcont= ($income*$percentage)+($income*$match)+$roth;

    $_SESSION['calc4']['totalcont'] = $totalcont;
  // }
// echo $totalcont;
//  -- - - - - - - - - - - - - 2nd set of calculations - - - - - - - - - - - --

if (isset($_SESSION['calc4']['round2'])) {

  $years = round($_POST['years']);
  $_SESSION['calc4']['years'] = $years;

  $retage= round($_POST['retage']);
  $_SESSION['calc4']['retage'] = $retage;

  $_SESSION['calc4']['initial'] = $_POST['initial'];
  $temp1 = preg_replace('/[\$,]/', '', $_POST['initial']);
  $temp1= floatval($temp1);
  $initial= round($temp1);

  $_SESSION['calc4']['interest'] = $_POST['interest'];
  $temp2 = preg_replace('/[\%,]/', '', $_POST['interest']);
  $temp2= floatval($temp2);
  $interest= round($temp2);

  // Validate input
    if($interest > 0)
    {
      $r= $interest / 100;
    } else {
      $_SESSION['error'][] = "Interest cannot be negative or 0";
      $validated = false;
    }
    if ($years <= 0)
    {
      $_SESSION['error'][] = "Years cannot be negative or 0";
      $validated = false;
    }
    if ($years > ($retage-$age))
    {
      $_SESSION['error'][] = "Years cannot be greater expected retirement age";
      $validated = false;
    }
    if ($retage <$age)
    {
      $_SESSION['error'][] = "Retirement age cannot be less than current age";
      $validated = false;
    }
// Years to compound without annual contributions
 $t= $retage-($age+$years);
// Calculate total value with monthly contributions

  $first = ($initial* pow((1+$r),$years))+ $totalcont*((pow((1+$r),($years+1))-(1+$r))/$r);
   $totalvalue= $first * pow((1+$r),$t);

    $_SESSION['calc4']['totalvalue']=round($totalvalue,2);
    // unset($_POST['calc2']);
}
//  -- - - - - - - - - - - - - - RESULTS-  SAVE THIS TO DATABASE!- - - - - - - - - - - - --
//}
//if logged in users have save to profile option
if (isset($_POST['save1'])) {
       # Save-button was clicked
       $dao->saveCalc4part1($username, $age, $income, $percentage, $match, $roth, $totalcont);
   }
if (isset($_POST['save2'])) {
      # Save-button was clicked
      $dao->saveCalc4part2($username, $initial, $interest, $years, $retage, $totalvalue);
  }

  // echo $status;
  header('Location: calc4.php');
?>
